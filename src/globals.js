const MONTH = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]

export const renderTimestamp = timestamp => {
  let tf = new Date()
    let ti = new Date(timestamp)
    let result = tf-ti
    let sec = result/1000
    sec = Math.round(sec)
    let min = sec/60
    min = Math.round(min)
    let hour = min/60
    hour = Math.round(hour)

    let m = ti.getMinutes()
      if(m <=9){
      
        m = '0'+m
        
      }

    let text = ''
    if(sec <=59){
      text = `${min} sec ago`
    }
    else if(sec >59 && min <=59){ 
      text = `${min} min ago`
    }
    else if(min > 59 && hour <=2){
      text = `${hour} hour ago`
    }
    else {
      if(ti.getFullYear() < tf.getFullYear()){
        text = `${MONTH[ti.getMonth()]} ${ti.getDate()}, ${ti.getFullYear()}  at ${ti.getHours()}:${m}`
      }
      else if(ti.getFullYear() === tf.getFullYear()){
        if(ti.getMonth() < tf.getMonth()){
          text = `${MONTH[ti.getMonth()]} ${ti.getDate()} at ${ti.getHours()}:${m}`
        }
        else if(ti.getMonth() === tf.getMonth()){
          if(ti.getDate() < tf.getDate()){
            if(ti.getDate()+1 === tf.getDate()){
              text = `Yesterday at ${ti.getHours()}:${m}`
            }
            else{
              text = `${MONTH[ti.getMonth()]} ${ti.getDate()} at ${ti.getHours()}:${m}`
            }
          }
          else if(ti.getDate()===tf.getDate()){
              text = `Today at ${ti.getHours()}:${m}`
          }
        }
      }
      
    }

    return text;
}