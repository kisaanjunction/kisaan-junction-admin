import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo' 
import { Provider } from 'rendition';
import { ThemeProvider } from '@smooth-ui/core-sc';
import theme from './theme';

let loggedIn = JSON.parse(localStorage.getItem('@user'))

const client = new ApolloClient({
  uri: 'https://api.kisaanjunction.com',
  headers: {
    'Authorization': loggedIn && loggedIn.token ? `Bearer ${loggedIn.token}` : 'None'
  }
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <Provider>
      <ThemeProvider theme={theme}>
        <App /> 
      </ThemeProvider>
    </Provider>
  </ApolloProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
