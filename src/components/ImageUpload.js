import React from 'react';
import styled from 'styled-components';
import DefaultImage from './../assets/default-image.jpg';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

const Container = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 10px;
`;

export const ImageOverlay = styled.div`
	position: absolute;
	top: 50%;
	left: 50%;
	opacity: ${(props) => (props.loading ? `1` : `0`)};
	transition: 0.5s ease;
	font-weight: 900;
	cursor: pointer;
	transform: translate(-50%, -50%);
  text-align: center;

	${Container}:hover & {
		opacity: 1;
	}
`;

export const Image = styled.img`
	width: ${(props) => (props.cover ? '100%' : '160px')};
	height: ${(props) => (props.cover ? '200px' : '160px')};
	transition: 0.5s ease;
	opacity: ${(props) => (props.loading ? `0.5` : `1`)};
	cursor: pointer;
	border-radius: ${(props) => (props.cover ? '0' : '100px')};
	object-fit: cover;

	${Container}:hover & {
		opacity: 0.5;
	}
`;

const GET_SIGNED_URL = gql`
  mutation ($key: String!){
    getSignedUrl(key: $key){
      signed,
      fileUrl
    }
  }
`

export default class ImageUpload extends React.Component {

  state = {
    loading: false,
    file: '',
    imageUplaoded: false,
    image: '',
    imageOverlayText: 'Change'
  }

  componentDidMount(){
    if(this.props.default.length > 0)
      this.setState({image: this.props.default})
  }

  formatFileName = filename => {
    let fileArr = filename.split('.');
    let fileExtension = fileArr[fileArr.length -1];
    fileArr.splice(-1);
    let fileName = fileArr.join('.');
   
    let uniqueFileName = `${fileName}-${new Date().valueOf()}.${fileExtension}`

    return uniqueFileName
  }

  _handleChange = (file, mutate) => {
    if(file){
      let fileName = this.formatFileName(file.name)
      this.setState({ loading: true, imageOverlayText: 'Uploading...', file })
      
      mutate({
        variables: {
          key: fileName
        }
      })
    }
  }

  _handleComplete = async data => {
    let url = data.getSignedUrl.signed;

    try{

      const resObj = await fetch(url, {
        method: 'put',
        headers: {
          'Content-Type': this.state.file.type
        },
        body: this.state.file
      });

      if(resObj.ok){
        this.setState({imageUploaded: true, loading: false, imageOverlayText: 'Change', image: data.getSignedUrl.fileUrl});
        this.props.onImageUploaded(data.getSignedUrl.fileUrl);
      }else{
        let res = await resObj.text();
        console.log(res)
      }
    }catch(err){
      console.log(err)
    }
  }

  render(){
    return (
      <Mutation mutation={GET_SIGNED_URL}
        onCompleted={data => this._handleComplete(data)}>
        {(getSignedUrl, {err, data, loading}) => (
          <Container>
            <input
              type="file"
              id="fileInput"
              style={{ position: 'absolute', height: '100%', width: '100%', opacity: 0 }}
              accept="image/*"
              onChange={e => this._handleChange(e.target.files[0], getSignedUrl)}
              disabled={this.state.loading}
            />
            <label htmlFor="fileInput">
              <Image
                src={this.state.image.length>0 ? this.state.image : DefaultImage}
                loading={this.state.loading} cover
              />
              <ImageOverlay loading={this.state.loading} cover>
                {this.state.imageOverlayText}
              </ImageOverlay>
            </label>
          </Container>
        )}
      </Mutation>
    );
  }
}

ImageUpload.defaultProps = {
  default : ''
}