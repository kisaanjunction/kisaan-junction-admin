import React from 'react';
import styled, { css } from 'styled-components';
import { PieChartAlt, Categories } from 'styled-icons/boxicons-solid';
import { UserFriends } from 'styled-icons/fa-solid/UserFriends';
import { ListUl } from 'styled-icons/fa-solid/ListUl';
import Logo from './../assets/logo_compressed.png';
import { Images } from 'styled-icons/icomoon/Images';

const Container = styled.div`
  height: 100vh;
  background-color: ${props => props.theme.primary};
  flex: 1;
  min-width: 220px;
  transition: all 0.3s;

  @media (max-width: 768px){
    position: absolute;
    top:0;
    left: 0;
    bottom: 0;
    width: 75%;
    margin-left: ${props => props.sidebar ? '0' : '-75%'};
    box-shadow: 2px 0 10px rgba(0,0,0,0.2);
    z-index: 5
  }
`;

const SidebarHeader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.theme.secondary};
`;

const List = styled.ul`
  margin: 0;
  padding: 0;
`

const ListItem = styled.li`
  list-style: none;
  padding: 15px;
  transition: all 0.3s;
  color: #fff;
  cursor: pointer;
  
  &:hover{
    background-color: rgba(255,255,255,0.2);
  }

  ${props => props.active && css`
    border-left: 4px solid #e6b31e;
    background-color: rgba(255, 255, 255, 0.25);
  `}
`;

export default class Sidebar extends React.Component {

  navigateTo(path){
    if(this.props.sidebar)
      this.props.toggle();

    this.props.history.push(path);
  }

  render(){
    return (
      <Container {...this.props} ref={e => this.sidebarContainer = e}>
        <SidebarHeader>
          <img src={Logo} style={{width: 200, height: 200}} alt="Kisaan-Junction-Logo"/>
        </SidebarHeader>
        <div style={{padding: '30px 0'}}>
          <List>
            <ListItem onClick={() => this.navigateTo('/')}
              active={this.props.location.pathname === '/'}>
              <PieChartAlt size={20} />
              &nbsp;&nbsp;Overview
            </ListItem>
            <ListItem onClick={() => this.navigateTo('/categories')}
              active={this.props.location.pathname === '/categories'}>
              <Categories size={20} />
              &nbsp;&nbsp;Categories
            </ListItem>
            <ListItem onClick={() => this.navigateTo('/products')}
              active={this.props.location.pathname === '/products'}>
              <ListUl size={20} />
              &nbsp;&nbsp;Products
            </ListItem>
            <ListItem onClick={() => this.navigateTo('/users')}
              active={this.props.location.pathname === '/users'}>
              <UserFriends size={20} />
              &nbsp;&nbsp;Users
            </ListItem>
            <ListItem onClick={() => this.navigateTo('/photos')}
              active={this.props.location.pathname === '/photos'}>
              <Images size={20} />
              &nbsp;&nbsp;Photos
            </ListItem>
          </List>
        </div>
      </Container>
    )
  }
}