import React from 'react';
import styled from 'styled-components';
import ReactLoader from 'react-loader-spinner';

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`

export default class Loader extends React.Component {

  render(){
    return (
      <Container>
        <ReactLoader 
          type="Puff"
          color="#343434"
          height="70"	
          width="70"
        />   
      </Container>
    )
  }
}