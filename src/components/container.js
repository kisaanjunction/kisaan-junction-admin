import styled from 'styled-components';

const Container = styled.div`
  height: calc(100vh - 105px);
  overflow: auto;
  padding: 20px;
  background-color: #f7f8f9;
`;

export default Container;