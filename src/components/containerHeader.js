import styled from 'styled-components';

const ContainerHeader = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px 10px 10px 0;
  flex-wrap: wrap;
`

export default ContainerHeader;