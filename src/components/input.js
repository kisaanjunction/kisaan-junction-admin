import styled from 'styled-components';
import { Input as StyledInput } from 'rendition';

const Input = styled(StyledInput)`
  box-sizing: border-box;
  width: ${props => props.full ? '100%' : 'auto'};
  &:focus {
    border: 1px solid seagreen;
  }
`;

export default Input;