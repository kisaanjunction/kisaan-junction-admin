import React from 'react';
import styled, { css } from 'styled-components';
import { Menu, LogOut } from 'styled-icons/feather';
import { Button as StyledButton } from '@smooth-ui/core-sc';

const Container = styled.div`
  display: flex;
  height: 65px;
  justify-content: flex-end;
  background-color: ${props => props.theme.primary};
  padding: 0 25px 0 10px;

  @media(max-width: 768px){
    justify-content: space-between;
  }
`;

const Button = styled(StyledButton)`
  border: 0;
  background: transparent;
  color: #fff;
  cursor: pointer;
  transition: all 0.2s;
  &:hover{
    background-color: ${props => props.theme.secondary};
    color: white !important;
  }

  ${props => props.toggler && css`
    display: none;
    @media(max-width: 768px){
      display: block;
  `}
  }
`;

const LogoutButton = styled(Button)`
  color: tomato;
  &:hover{
    background-color: tomato;
    color: white;
  }
`


export default class Header extends React.Component {

  handleLogout = () => {
    localStorage.removeItem('@user');
    this.props.history.push('/login');
  }

  render(){
    return (
      <Container>
        <Button onClick={() => this.props.toggleSidebar()} toggler><Menu size="28"/></Button>
        <LogoutButton onClick={this.handleLogout} >
          <LogOut size="24"/> Log Out
        </LogoutButton>
      </Container>

    );
  }
}