import React from 'react';
import styled from 'styled-components';
import { Typography, Modal, ModalDialog, ModalContent, ModalBody } from '@smooth-ui/core-sc';
import { gql } from 'apollo-boost';
import { Query, Mutation } from 'react-apollo';
import ContainerHeader from './../../components/containerHeader';
import Container from './../../components/container';
import Loader from './../../components/loader';
import { Add } from 'styled-icons/material/Add';
import { Trash } from 'styled-icons/fa-solid/Trash';
import DeletePhoto from './deletePhoto';

const Grid = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const ContainerWrap = styled.div`
  margin: 20px 10px;
  border-radius: 10px;
  box-shadow: 0 0px 15px #111;
  background-color: #eee;
  width: 330px;
  transition: all 0.5s;
  height: 200px;
  position: relative;

  &:hover {
    box-shadow: 0 0 50px #111;
  }

  @media(max-width: 720px) {
    width: 100%;
  }
`;

const Label = styled.label`
  padding: .375rem .75rem;
  z-index: 1;
  cursor: pointer;
  font-size: 1rem;
  border-radius: .25rem;
  line-height: 1.5;
  color: #111;
  background-color: #e6b31e;
  text-align: center;
`;

const StyledTrash = styled(Trash)`
  position: absolute;
  top: -10px;
  right: -10px;
  color: tomato;
  background-color: rgba(255,255,255,0.9);
  padding: 7px;
  border-radius: 10px;
  cursor: pointer;
`

export const GET_PHOTOS = gql`
  {
    getPhotos{
      _id
      url
    }
  }
`;

const GET_SIGNED_URL = gql`
  mutation ($key: String!){
    getSignedUrl(key: $key){
      signed,
      fileUrl
    }
  }
`;

const ADD_PHOTO = gql`
  mutation($url: String!){
    addPhoto(url: $url){
      _id
      url
    }
  }
`

export default class Photos extends React.Component {
  
  state = {
    loading: false,
    file: {},
    deleteModal: false,
    photo: ''
  }

  formatFileName = filename => {
    let fileArr = filename.split('.');
    let fileExtension = fileArr[fileArr.length -1];
    fileArr.splice(-1);
    let fileName = fileArr.join('.');
   
    let uniqueFileName = `${fileName}-${new Date().valueOf()}.${fileExtension}`

    return uniqueFileName
  }

  _handleChange = (file, mutate) => {
    if(file){
      let fileName = this.formatFileName(file.name)
      this.setState({ loading: true, file })
      
      mutate({
        variables: {
          key: fileName
        }
      })
    }
  }

  _handleComplete = async (data, addPhoto) => {
    let url = data.getSignedUrl.signed;

    try{

      const resObj = await fetch(url, {
        method: 'put',
        headers: {
          'Content-Type': this.state.file.type
        },
        body: this.state.file
      });

      if(resObj.ok){
        addPhoto({
          variables: {
            url: data.getSignedUrl.fileUrl
          }
        })
      }else{
        let res = await resObj.text();
        console.log(res)
      }
    }catch(err){
      console.log(err)
    }
  }

  render(){
    return (
      <Mutation mutation={ADD_PHOTO} onCompleted={() => this.setState({loading: false})}
        refetchQueries={() => [{query: GET_PHOTOS}]}>
        {(addPhoto, { error, loading, data }) => (

          <Mutation mutation={GET_SIGNED_URL}
            onCompleted={data => this._handleComplete(data, addPhoto)}>
            {(getSignedUrl, { error, loading, data }) => (

              <Container>
                <ContainerHeader>
                  <Typography variant="h4" style={{color: '#343434'}}>Photos</Typography>
                  <input type="file" id="photoInput" accept="image/*" style={{width: 0, height: 0}}
                    onChange={e => this._handleChange(e.target.files[0], getSignedUrl)} />
                  <Label htmlFor="photoInput">
                      <Add size={24}/> Add New Photo
                  </Label>
                </ContainerHeader>

                <Query query={GET_PHOTOS}>
                  {({ loading, data, error }) => {
                    if(loading) return <Loader />
                    if(error) return <p>{error.message}</p>
                    if(data.getPhotos.length === 0)
                    return <p>No Photos Yet! </p>
                    
                    return <Grid>
                      {data.getPhotos.map(photo => 
                        <ContainerWrap key={photo._id}>
                          <img src={photo.url} style={{height: '100%', width: '100%', objectFit: 'cover'}} alt="Uploaded" />
                          <StyledTrash size={18} onClick={() => this.setState({ photo:  photo._id, deleteModal: true })}/>
                        </ContainerWrap>
                      )}
                    </Grid> 
                  }}
                </Query>

                <Modal opened={this.state.loading} onClose={() => {}}>
                  <ModalDialog>
                    <ModalContent>
                      <ModalBody>
                        <Loader />
                        <p style={{textAlign: 'center'}}>Uploading...</p>
                      </ModalBody>
                    </ModalContent>
                  </ModalDialog>
                </Modal>

                <DeletePhoto opened={this.state.deleteModal} onClose={() => this.setState({ deleteModal: false })}
                  id={this.state.photo} />

              </Container>
            )}
          </Mutation>
        )}
      </Mutation>
    );
  }
}