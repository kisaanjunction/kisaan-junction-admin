import React from 'react';
import { Modal, ModalDialog, ModalContent, ModalCloseButton, ModalHeader,
  ModalBody, ModalFooter, Typography, Button, Alert } from '@smooth-ui/core-sc';
import { Mutation } from 'react-apollo';
import { gql } from 'apollo-boost';
import { Loader } from 'styled-icons/feather';
import { GET_PHOTOS } from './index';

const DELETE_PHOTO = gql`
  mutation($photo: String!) {
    deletePhoto(photo: $photo) {
      _id
      url
    }
  }
`

export default class DeletePhoto extends React.Component {

  _handleComplete = () => {
    this.props.onClose();
  }

  handleSubmit = (e, deletePhoto) => {
    e.preventDefault();

    deletePhoto({
      variables: {
        photo: this.props.id
      }
    })
  }

  render(){
    return (
      <Mutation mutation={DELETE_PHOTO}
      onCompleted={data => this._handleComplete()}
      refetchQueries={() => [{query: GET_PHOTOS}]}>
        {(deletePhoto, { loading, data, error }) => (
          <Modal opened={this.props.opened} onClose={() => this.props.onClose()}>
            <ModalDialog>
              <ModalContent>
                <ModalCloseButton />
                <ModalHeader>
                  <Typography variant="h5" style={{color: '#343434'}}>
                    Delete Photo
                  </Typography>
                </ModalHeader>
                <ModalBody>
                  {error && <Alert variant="danger">{error.message}</Alert>}
                  <p style={{fontSize: 20, margin: 0}}>Are you sure ?</p>
                  <p>
                    Once you delete a photo, it won't be visible in website.   
                  </p>
                </ModalBody>
                <ModalFooter>
                  <Button variant="secondary" type="button" onClick={() => this.props.onClose()}>No</Button>
                  <Button variant="primary" onClick={e => this.handleSubmit(e, deletePhoto)}>
                    {loading ? <Loader size={24} /> : 'Yes'}
                  </Button>
                </ModalFooter>
              </ModalContent>
            </ModalDialog>
          </Modal>
        )}
      </Mutation>
    )
  }
}