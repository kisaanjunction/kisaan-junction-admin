import React, { Fragment } from 'react';
import { Box, Button, Input, FormGroup, Alert, Typography } from '@smooth-ui/core-sc'
//import Input from './../../components/input';
import { gql } from 'apollo-boost';
import { Mutation } from 'react-apollo';
import { LogIn, Loader } from 'styled-icons/feather';
import Logo from './../../assets/logo_compressed.png';
import Back from './../../assets/hero-back.jpg';

const loginMutation = gql`
  mutation($name: String!, $email: String!, $password: String!){
    loginAdmin(name: $name, email: $email, password: $password){
      token
      user{
        _id
        name
        email
      }
    }
  }
`

export default class Login extends React.Component {

  state = {
    name: '',
    email: '',
    password: ''
  }

  componentDidMount(){
    let loggedIn = JSON.parse(localStorage.getItem('@user'));

    if(loggedIn && loggedIn.token && loggedIn.user)
      window.location.href = '/'
  }

  _handleSubmit(e, mutation ){
    e.preventDefault();

    mutation({
      variables: {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password
      }
    })
  }

  _handleComplete(data){
    localStorage.setItem('@user', JSON.stringify(data.loginAdmin))
    window.location.href = '/'
  }

  render(){
    return (
      <Mutation mutation={loginMutation}
        onCompleted={data => this._handleComplete(data)}>
        {(loginAdmin, {loading, data, error}) => (
          <Fragment>
          <div style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, zIndex: -2}}>
            <img src={Back} style={{width: '100%', height: '100%', objectFit: 'cover'}} alt="Background"/>
          </div>
          <div style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, zIndex: -1, backgroundColor: 'rgba(0,0,0,0.5)'}} />
          <Typography variant="h1" style={{color: '#fff', marginTop: '3%', position: 'absolute', left: '50%', transform: 'translateX(-50%)'}}>Admin&nbsp;Login</Typography>
          <Box style={styles.container}>
            <Box style={{alignSelf: 'center'}}>
              <img src={Logo} style={{ height:250}} alt="Logo" />
              <form onSubmit={(e) => this._handleSubmit(e, loginAdmin)}>
                {error && <Alert variant="danger">{error.message}</Alert>}
                <FormGroup>
                  <Input style={{boxSizing: 'border-box'}} type="text" placeholder="Full Name" control
                    onChange={e => this.setState({name: e.target.value})} value={this.state.name}
                    required />
                </FormGroup>
                <FormGroup>
                  <Input style={{boxSizing: 'border-box'}} type="email" placeholder="E-mail" control
                    onChange={e => this.setState({email: e.target.value})} value={this.state.email}
                    required />
                </FormGroup>
                <FormGroup>
                  <Input style={{boxSizing: 'border-box'}} type="password" placeholder="password" control
                    onChange={e => this.setState({password: e.target.value})} value={this.state.password}
                    required />
                </FormGroup>
                <Button type="submit" disabled={loading} variant="secondary" style={{width:"100%"}}> 
                  {loading ? <Loader size={18} /> : <Fragment>
                      Log In <LogIn size={18} style={{color:'#000'}}/>
                    </Fragment>}
                </Button>
              </form>
            </Box>
          </Box>
          </Fragment>
        )}
      </Mutation>
    )
  }
} 

const styles = {
  container: {
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
    flexDirection: 'column',
    height: '100vh',
    justifyContent: 'center'
  }
}