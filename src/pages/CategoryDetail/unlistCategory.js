import React from 'react';
import { Modal, ModalDialog, ModalContent, ModalCloseButton, ModalHeader,
  ModalBody, ModalFooter, Typography, Button, Alert } from '@smooth-ui/core-sc';
import { Mutation } from 'react-apollo';
import { gql } from 'apollo-boost';
import { Loader } from 'styled-icons/feather';
import { GET_CATEGORIES, GET_UNLISTED_CATEGORIES } from './../categories';

const UNLIST_CATEGORY = gql`
  mutation($categoryId: String!) {
    unlistCategory(categoryId: $categoryId) {
      name
      photoURL
    }
  }
`

export default class UnlistCategory extends React.Component {

  handleSubmit(e, unlistCategory){
    e.preventDefault();

    unlistCategory({
      variables: {
        categoryId: this.props.id
      }
    })
  }

  _handleComplete = () => {
    this.props.onClose();
    this.props.history.goBack()
  }

  render(){
    return (
      <Mutation mutation={UNLIST_CATEGORY}
        onCompleted={data => this._handleComplete()}
        refetchQueries={() => [{query: GET_CATEGORIES}, {query: GET_UNLISTED_CATEGORIES}]}>
      {(unlistCategory, { loading, data, error }) => (
        <Modal opened={this.props.opened} onClose={() => this.props.onClose()}>
          <ModalDialog>
            <ModalContent>
              <ModalCloseButton />
              <ModalHeader>
                <Typography variant="h5" style={{color: '#343434'}}>
                  {this.props.unlisted ? 'Re-list Category' : 'Unlist Category' }
                </Typography>
              </ModalHeader>
              <ModalBody>
                {error && <Alert variant="danger">{error.message}</Alert>}
                <p style={{fontSize: 20, margin: 0}}>Are you sure ?</p>
                <p>
                  {
                    this.props.unlisted ? "Once you re-listed a category, it will be visible in the app again"
                    : "Once you unlisted a category, it won't be visible in the app"
                  }    
                </p>
              </ModalBody>
              <ModalFooter>
                <Button variant="secondary" type="button" onClick={() => this.props.onClose()}>No</Button>
                <Button variant="primary" onClick={e => this.handleSubmit(e, unlistCategory)}>
                  {loading ? <Loader size={24} /> : 'Yes'}
                </Button>
              </ModalFooter>
            </ModalContent>
          </ModalDialog>
        </Modal>
        )}
      </Mutation>
    );
  }
}