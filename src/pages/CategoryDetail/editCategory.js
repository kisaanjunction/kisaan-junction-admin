import React from 'react';
import { Modal, ModalDialog, ModalContent, ModalCloseButton, ModalHeader,
  ModalBody, ModalFooter, Typography, FormGroup, Input, Label,
  Button, Alert } from '@smooth-ui/core-sc';
import { Mutation } from 'react-apollo';
import { gql } from 'apollo-boost';
import { Loader } from 'styled-icons/feather';
import { GET_CATEGORIES } from './../categories';
import ImageUpload from './../../components/ImageUpload';

const EDIT_CATEGORY = gql`
  mutation($categoryId: String!, $name: String, $photoURL: String) {
    updateCategory(categoryId: $categoryId, name: $name, photoURL: $photoURL) {
      name
      photoURL
    }
  }
`

export default class EditCategory extends React.Component {

  state = {
    name: '',
    photoURL: ''
  }

  componentDidMount(){
    this.setState({
      name: this.props.name,
      photoURL: this.props.photoURL
    })
  }

  handleSubmit(e, editCategory){
    e.preventDefault();

    if(this.state.name.length > 0 && this.state.photoURL.length > 0)
      editCategory({
        variables: {
          categoryId: this.props.id,
          name: this.state.name,
          photoURL: this.state.photoURL
        }
      })
    else
      return alert('Please Upload Image')
  }

  _handleImageUploaded = url => {
    this.setState({ photoURL: url })
  }

  render(){
    return (
      <Mutation mutation={EDIT_CATEGORY}
        onCompleted={data => this.props.toggle()}
        refetchQueries={() => [{query: GET_CATEGORIES}]}>
      {(editCategory, { loading, data, error }) => (
        <Modal opened={this.props.opened} onClose={() => this.props.toggle()}>
          <ModalDialog>
            <ModalContent>
              <ModalCloseButton />
              <ModalHeader>
                <Typography variant="h5" style={{color: '#343434'}}>Edit Category</Typography>
              </ModalHeader>
              <form onSubmit={e => this.handleSubmit(e, editCategory)}>
                <ModalBody>
                  {error && <Alert variant="danger">{error.message}</Alert>}
                  <FormGroup>
                    <Label>Category Name</Label>
                    <Input style={{boxSizing: 'border-box'}} autoFocus required
                      type="text" placeholder="Eg (Fruits)" control
                      value={this.state.name} onChange={e => this.setState({ name: e.target.value })}/>
                  </FormGroup>
                  <FormGroup>
                    <Label>Choose Image</Label>
                    <ImageUpload onImageUploaded={this._handleImageUploaded} default={this.props.photoURL} />
                  </FormGroup>
                </ModalBody>
                <ModalFooter>
                  <Button variant="secondary" type="button" onClick={() => this.props.toggle()}>Cancel</Button>
                  <Button variant="primary" type="submit">
                    {loading ? <Loader size={24} /> : 'Submit'}
                  </Button>
                </ModalFooter>
              </form>
            </ModalContent>
          </ModalDialog>
        </Modal>
        )}
      </Mutation>
    );
  }
}