import React, { Fragment } from 'react';
import ContainerHeader from './../../components/containerHeader';
import { Toggler, Typography, Box, Button } from '@smooth-ui/core-sc';
import Container from './../../components/container';
import styled from 'styled-components';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';
import { ModeEdit } from 'styled-icons/material/ModeEdit';
import { Hide } from 'styled-icons/boxicons-solid/Hide';
import { Show } from 'styled-icons/boxicons-regular/Show'
import EditCategory from './editCategory';
import Loader from './../../components/loader';
import UnlistCategory from './unlistCategory';
import Product from './../products/product';

const StyledBox = styled(Box)`
  display: flex;
  flex-direction: column;
  background-color: #fff;
  border-radius: 8px;
  box-shadow: 0 1px 15px #ddd;
  padding: 20px;
  margin: 20px 0;
`;

const Grid = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const Img = styled.img`
  width: 300px;
  max-width: 100%;
  height: 200px;
  object-fit: cover;
  border-radius: 5px;
`

const GET_CATEGORY = gql`
  query($category: String!){
    getCategory(category: $category){
      _id
      name
      photoURL
      createdAt
      lastModified
      unlisted
      products {
        _id
        title
        price
        category{
          name
        }
        attachments{
          url
        }
        createdAt
      }
    }
  }
`

export default class CategoryDetail extends React.Component {

  state = {
    unlistModal: false
  }

  render(){
    return (
      <Container>
        <Query query={GET_CATEGORY} variables={{category: this.props.match.params.categoryName}}>
          {({ loading, data, error }) => {
            if(loading) return <Loader />
            if(error) return <p>{error.message}</p>

            return (
              <Fragment>
                
                <ContainerHeader>
                  <Typography variant="h4" style={{color: '#343434'}}>Category Details</Typography>
                  <Toggler>
                    {({ toggled, onToggle }) => (
                      <div>
                        <Button variant="secondary" onClick={() =>  onToggle(true)}>
                          <ModeEdit size={16}/> Edit Category
                        </Button>

                        <Button variant="danger" style={{margin: '0 0px 0px 10px'}}
                          onClick={() => this.setState({ unlistModal: true })}>
                          {data.getCategory.unlisted ? <Show size={20} /> : <Hide size={16} />} 
                          {data.getCategory.unlisted ? ' Relist Category' : ' Unlist Category'}
                        </Button>
                        
                        <EditCategory opened={toggled} toggle={() => onToggle(false)} name={data.getCategory.name} 
                          photoURL={data.getCategory.photoURL} id={data.getCategory._id}
                        />

                        <UnlistCategory opened={this.state.unlistModal} onClose={() => this.setState({ unlistModal: false })}
                          id={data.getCategory._id} history={this.props.history} unlisted={data.getCategory.unlisted}/>
                      </div>
                    )}
                  </Toggler>
                </ContainerHeader>

                <StyledBox>
                  <Typography variant="h3" style={{color: '#343434'}}>
                    {data.getCategory.name}
                  </Typography>
                  <br/>
                  <Typography variant="h5" style={{color: '#343434'}}>
                    Background Image
                  </Typography>
                  <Img src={data.getCategory.photoURL} alt="Background Image"/>
                </StyledBox>

                <ContainerHeader>
                  <Typography variant="h4" style={{color: '#343434'}}>Products</Typography>
                </ContainerHeader>
                
                {
                  data.getCategory.products.length === 0 ? <p>No Products listed in this category</p> : 
                  <Grid>
                    {data.getCategory.products.map(item => (
                      <Product title={item.title} category={item.category.name} price={item.price} id={item._id}
                      image={item.attachments[0].url} attachments={item.attachments}
                      createdAt={item.createdAt}
                      />
                      ))}
                  </Grid>
                }
              </Fragment>
            )
          }}
        </Query>
      </Container>
    );
  }
}