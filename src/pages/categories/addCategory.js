import React from 'react';
import { Modal, ModalDialog, ModalContent, ModalCloseButton, ModalHeader,
  ModalBody, ModalFooter, Typography, FormGroup, Input, Label,
  Button, Alert } from '@smooth-ui/core-sc';
import { Mutation } from 'react-apollo';
import { gql } from 'apollo-boost';
import { Loader } from 'styled-icons/feather';
import { GET_CATEGORIES } from './index';
import ImageUpload from './../../components/ImageUpload';

const ADD_CATEGORY = gql`
  mutation($name: String!, $photoURL: String) {
    addCategory(name: $name, photoURL: $photoURL) {
      name
      photoURL
    }
  }
`

export default class AddCategory extends React.Component {

  state = {
    name: '',
    photoURL: ''
  }

  handleSubmit(e, addCategory){
    e.preventDefault();

    if(this.state.name.length > 0 && this.state.photoURL.length > 0)
      addCategory({
        variables: {
          name: this.state.name,
          photoURL: this.state.photoURL
        }
      })
    else
      return alert('Please Upload Image')
  }

  _handleImageUploaded = url => {
    this.setState({ photoURL: url })
  }

  render(){
    return (
      <Mutation mutation={ADD_CATEGORY}
        onCompleted={data => this.props.toggle()}
        refetchQueries={() => [{query: GET_CATEGORIES}]}>
      {(addCategory, { loading, data, error }) => (
        <Modal opened={this.props.opened} onClose={() => this.props.toggle()}>
          <ModalDialog>
            <ModalContent>
              <ModalCloseButton />
              <ModalHeader>
                <Typography variant="h5" style={{color: '#343434'}}>Add Category</Typography>
              </ModalHeader>
              <form onSubmit={e => this.handleSubmit(e, addCategory)}>
                <ModalBody>
                  {error && <Alert variant="danger">{error.message}</Alert>}
                  <FormGroup>
                    <Label>Category Name</Label>
                    <Input style={{boxSizing: 'border-box'}} autoFocus required
                      type="text" placeholder="Eg (Fruits)" control
                      value={this.state.name} onChange={e => this.setState({ name: e.target.value })}/>
                  </FormGroup>
                  <FormGroup>
                    <Label>Choose Image</Label>
                    <ImageUpload onImageUploaded={this._handleImageUploaded} />
                  </FormGroup>
                </ModalBody>
                <ModalFooter>
                  <Button variant="secondary" type="button" onClick={() => this.props.toggle()}>Cancel</Button>
                  <Button variant="primary" type="submit">
                    {loading ? <Loader size={24} /> : 'Submit'}
                  </Button>
                </ModalFooter>
              </form>
            </ModalContent>
          </ModalDialog>
        </Modal>
        )}
      </Mutation>
    );
  }
}