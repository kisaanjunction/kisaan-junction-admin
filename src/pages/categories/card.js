import React from 'react';
import { Typography } from '@smooth-ui/core-sc';
import styled from 'styled-components';

const Container = styled.div`
  margin: 15px;
  height: 150px;
  min-width: 280px;
  flex: 1;
  cursor: pointer;
  transition: all 0.5s;
  overflow: hidden;
  border-radius: 8px;
  box-shadow: 0 2px 15px #444;
`;

const InnerContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
  background-color: #999;
  background-image: url('${props => props.photoURL}');
  background-size: cover;
  background-position: center;
  transition: all 0.5s;

  ${Container}:hover & {
    transform: scale(1.2);
  }
`

const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
`;

export default class Card extends React.Component {

  render(){
    return (
      <Container onClick={() => this.props.history.push(`/categories/${this.props.id}`)}>
        <InnerContainer photoURL={this.props.photoURL}>
          <Overlay />
          <Typography variant="h4" style={{color: '#fff', zIndex: 1}}>{this.props.name}</Typography>
        </InnerContainer>
      </Container>
    );
  }
}

Card.defaultProps = {
  name: 'Category Name'
}