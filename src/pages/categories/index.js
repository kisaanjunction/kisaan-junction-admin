import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Toggler, Typography, Button } from '@smooth-ui/core-sc';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo'
import Card from './card';
import { Add } from 'styled-icons/material/Add';
import ContainerHeader from './../../components/containerHeader';
import Container from './../../components/container';
import AddCategory from './addCategory';
import Loader from './../../components/loader';

const Grid = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const GET_CATEGORIES = gql`
  {
    getCategories{
      _id,
      name,
      photoURL
      unlisted
    }
  }
`

export const GET_UNLISTED_CATEGORIES = gql`
  {
    getUnlistedCategories{
      _id
      name
      photoURL
      unlisted
    }
  }
`

export default class Categories extends React.Component {

  render(){
    return (
      <Container>
        <ContainerHeader>
          <Typography variant="h4" style={{color: '#343434'}}>Categories</Typography>
          <Toggler>
            {({ toggled, onToggle }) => (
              <Fragment>
                <Button variant="secondary" onClick={() => onToggle(true)}>
                  <Add size={24}/> Add Category
                </Button>
                <AddCategory opened={toggled} toggle={() => onToggle(false)}/>
              </Fragment>
            )}
          </Toggler>
        </ContainerHeader>

        <Query query={GET_CATEGORIES}>
            {({ loading, data, error }) => {
              
              if(loading) return <Loader />
              if(error) return <p>{error.message}</p>
              return ( 
                <Grid>
                  {data.getCategories.map(category => (
                    <Card photoURL={category.photoURL} name={category.name} 
                      key={category._id} id={category._id} history={this.props.history} />
                  ))}
                </Grid>
              ) 
            }}
        </Query>

        <ContainerHeader>
          <Typography variant="h4" style={{ color: '#343434'}}>Unlisted Categories</Typography>
        </ContainerHeader>

        <Query query={GET_UNLISTED_CATEGORIES}>
            {({ loading, data, error }) => {
              
              if(loading) return <Loader />
              if(error) return <p>{error.message}</p>

              if(data.getUnlistedCategories.length === 0)
                return <p> No Unlisted Categories </p>
              return ( 
                <Grid>
                  {data.getUnlistedCategories.map(category => (
                    <Card photoURL={category.photoURL} name={category.name} 
                      key={category._id} id={category._id} history={this.props.history} />
                  ))}
                </Grid>
              ) 
            }}
        </Query>
      </Container>
    );
  }
}