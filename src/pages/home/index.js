import React from 'react';
import { Box } from '@smooth-ui/core-sc'; 
import Sidebar from './../../components/sidebar';
import { Switch, Route, withRouter } from 'react-router-dom';
import Overview from './../overview';
import Categories from './../categories';
import CategoryDetail from './../CategoryDetail';
import Users from './../users';
import styled from 'styled-components';
import Header from './../../components/header';
import Products from './../products';
import Photos from './../photos';

const Content = styled.div`
  flex: 5
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-color: rgba(0,0,0,0.4);
  z-index:1;
  transition: all 0.2s;
`

class Home extends React.Component {

  state = {
    sideBar: false
  }

  componentDidMount(){
    let loggedIn = JSON.parse(localStorage.getItem('@user'));

    if(!loggedIn || !loggedIn.token || !loggedIn.user)
      this.props.history.push('/login');
  }

  render(){
    return (
      <Box style={styles.container}>
        <Sidebar {...this.props} sidebar={this.state.sideBar} 
          toggle={() => this.setState({sideBar: !this.state.sideBar})}/>
        {this.state.sideBar && <Overlay onClick={() => this.setState({sideBar: false})}/>} 
        <Content>
          <Header {...this.props} 
            toggleSidebar={() => this.setState({sideBar: !this.state.sideBar})}/>
          <Switch>
            <Route exact path='/' component={Overview}/>
            <Route exact path="/categories/:categoryName" component={CategoryDetail} />  
            <Route path="/categories" component={Categories} />
            <Route path="/products" component={Products} />
            <Route path="/users" component={Users} />
            <Route path="/photos" component={Photos} />
          </Switch>
        </Content>
      </Box>
    );
  }
}

const styles = {
  container: {
    display: 'flex'
  }
}

export default withRouter(Home)