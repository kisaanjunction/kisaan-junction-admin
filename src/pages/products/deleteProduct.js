import React from 'react';
import { Modal, ModalDialog, ModalContent, ModalCloseButton, ModalHeader,
  ModalBody, ModalFooter, Typography, Button, Alert } from '@smooth-ui/core-sc';
import { Mutation } from 'react-apollo';
import { gql } from 'apollo-boost';
import { Loader } from 'styled-icons/feather';
import { GET_PRODUCTS } from './index';

const DELETE_PRODUCT = gql`
  mutation ($product: String!) {
    deleteProductAdmin(product: $product) {
      _id
    }
  }
`

export default class DeleteProduct extends React.Component {
  
  handleSubmit(e, deleteProduct){
    e.preventDefault();

    deleteProduct({
      variables: {
        product: this.props.id
      }
    })
  }

  _handleComplete = () => {
    this.props.onDelete();
    //this.props.history.goBack()
  }

  render(){
    return (
      <Mutation mutation={DELETE_PRODUCT}
        onCompleted={data => this._handleComplete()}
        refetchQueries={() => [{query: GET_PRODUCTS}]}>
      {(deleteProduct, { loading, data, error }) => (
        <Modal opened={this.props.opened} onClose={() => this.props.onClose()}>
          <ModalDialog>
            <ModalContent>
              <ModalCloseButton />
              <ModalHeader>
                <Typography variant="h5" style={{color: '#343434'}}>
                  Delete Product
                </Typography>
              </ModalHeader>
              <ModalBody>
                {error && <Alert variant="danger">{error.message}</Alert>}
                <p style={{fontSize: 20, margin: 0}}>Are you sure ?</p>
                <p>
                  Once you deleted a product, it cannot be undone.
                </p>
              </ModalBody>
              <ModalFooter>
                <Button variant="secondary" type="button" onClick={() => this.props.onClose()}>No</Button>
                <Button variant="primary" onClick={e => this.handleSubmit(e, deleteProduct)}>
                  {loading ? <Loader size={24} /> : 'Yes'}
                </Button>
              </ModalFooter>
            </ModalContent>
          </ModalDialog>
        </Modal>
        )}
      </Mutation>
    )
  }
}