import React from 'react';
import styled from 'styled-components';
import { renderTimestamp } from './../../globals';
import PlaceholderImage from './../../assets/default-image.jpg';
import ProductDetail from './productDetail';

const Container = styled.div`
  margin: 20px 10px;
  border-radius: 10px;
  box-shadow: 0 0px 15px #ccc;
  background-color: #eee;
  width: 330px;
  cursor: pointer;
  transition: all 0.5s;

  &:hover {
    box-shadow: 0 0 50px #aaa;
  }

  @media(max-width: 720px) {
    width: 100%;
  }
`;

const ContainerHead = styled.div`
  height: 200px
`;

const ContainerBottom = styled.div`
  padding: 10px;
  background-color: #fff;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
  display: flex;
  flex-direction: column;
`;

const StyledImage = styled.img`
  width: 100%;
  height: 100%;
  resize-mode: cover;
  border-radius: 10px;
`

export const Title = styled.span`
  color: #222;
  font-size: 20px;
  font-family: Arial;
  margin: 10px 0;
`;

export const SubTitle = styled.span`
  color: #12902c;
  font-size: 18px;
  font-family: Arial;
`

const Category = styled.div`
  border: 1px solid #999;
  background-color: transparent;
  color: #fff;
  border-radius: 50px;
  padding: 3px 10px;
`

export default class Product extends React.Component {

  state = {
    modal: false
  }

  render(){
    return (
      <Container onClick={() => this.setState({ modal: true })}>
        <ProductDetail opened={this.state.modal} onClose={() => this.setState({ modal: false })}
          attachments={this.props.attachments} title={this.props.title} price={this.props.price}
          id={this.props.id} />
        <ContainerHead>
          <StyledImage src={this.props.image} />
        </ContainerHead>
        <ContainerBottom>
          <div style={{display: 'flex'}}>
            <Category>
              <span style={{fontSize: 11, color: '#666'}}>{this.props.category}</span>
            </Category>
          </div>
          <Title>{this.props.title}</Title>
          <SubTitle>₹ {this.props.price}</SubTitle>
          <span>{renderTimestamp(this.props.createdAt)}</span>
        </ContainerBottom>
      </Container>
    )
  }
}

Product.defaultProps = {
  title: 'Title',
  image: PlaceholderImage,
  price: '1000',
  category: 'Category',
  onPress: () => {}
}