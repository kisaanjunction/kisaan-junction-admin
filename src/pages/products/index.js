import React from 'react';
import styled from 'styled-components';
import ContainerHeader from './../../components/containerHeader';
import Container from './../../components/container';
import { Typography } from '@smooth-ui/core-sc';
import { Query } from 'react-apollo';
import gql from 'graphql-tag'; 
import Product from './product';
import Loader from './../../components/loader';

const Grid = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const GET_PRODUCTS = gql`
  query ($offset: Int){
    getProducts(offset: $offset) {
      _id
      title
      price
      category{
        name
      }
      attachments{
        url
      }
      createdAt
    }
  }
`

export default class Products extends React.Component {

  render(){
    return (
      <Container>
        <ContainerHeader>
          <Typography variant="h4" style={{color: '#343434'}}>Products</Typography>
        </ContainerHeader>
      
        <Query query={GET_PRODUCTS} pollInterval={5000}>
          {({ loading, data, error }) => {
            
            if(loading) return <Loader />
            if(error) return <p>{error.message}</p>
            return ( 
              <Grid>
                {data.getProducts.map(item => (
                  <Product title={item.title} category={item.category.name} price={item.price} id={item._id}
                    image={item.attachments[0].url} attachments={item.attachments}
                    createdAt={item.createdAt}
                  />
                ))}
              </Grid>
            ) 
          }}
        </Query>
      </Container>
    )
  }
}