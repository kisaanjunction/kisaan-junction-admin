import React, { Fragment } from 'react';
import { Modal, ModalDialog, ModalContent, ModalCloseButton,
  ModalBody, ModalFooter, Button } from '@smooth-ui/core-sc';
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost';
import Slider from "react-slick";
import { Title, SubTitle } from './product';
import LoaderComponent from './../../components/loader';
import { Trash } from 'styled-icons/boxicons-solid/Trash';
import DeleteProduct from './deleteProduct'

const GET_PRODUCT = gql`
query($product: String!){
  getProduct(product: $product){
    description
    user{
      _id
      name
      phone
      email
      state
      district
      tehsil
      village
    }
  }
}
`

export default class ProductDetail extends React.Component {

  state = {
    unlistModal: false
  }

  _handleDelete = () => {
    this.setState({unlistModal: false})
    this.props.onClose()
  }

  render(){
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return (
      <Query query={GET_PRODUCT} variables={{product: this.props.id}}>
      {({ loading, data, error }) => (
        <Fragment>
          <Modal opened={this.props.opened} onClose={() => this.props.onClose()}>
            <ModalDialog>
              <ModalContent>
                <ModalCloseButton />
                <Slider {...settings}>
                  {this.props.attachments.map(attachment => (
                    <div style={{height: 250}}>
                      <img src={attachment.url} style={{height: '250px', width: '100%', objectFit: 'cover'}} alt="attachments" />
                    </div>
                  ))}
                </Slider>
                <ModalBody style={{display: 'flex', flexDirection: 'column'}}>
                  <Title>{this.props.title}</Title>
                  <SubTitle>₹ {this.props.price}</SubTitle>
                  {
                    loading ? <LoaderComponent /> : error ? <p>{error.message}</p> :
                      <Fragment>
                        <p style={{fontWeight: 'bold', marginBottom: 0}}>Description</p>
                        <p style={{whiteSpace: 'pre-wrap'}}>{data.getProduct.description}</p>
                        <p style={{fontWeight: 'bold', marginBottom: 0}}>Posted By:</p>
                        <p style={{marginBottom: 0}}>Name: {data.getProduct.user.name}</p>
                        <p style={{margin: 0}}>E-mail: {data.getProduct.user.email}</p>
                        <p style={{margin: 0}}>Phone: {data.getProduct.user.phone}</p>
                        <p style={{margin: 0}}>Village: {data.getProduct.user.village}</p>
                        <p style={{margin: 0}}>Tehsil: {data.getProduct.user.tehsil}</p>
                        <p style={{margin: 0}}>District: {data.getProduct.user.district}</p>
                        <p style={{margin: 0}}>State: {data.getProduct.user.state}</p>
                      </Fragment>
                  }
                </ModalBody>
                <ModalFooter>
                  <Button variant="danger" onClick={() => this.setState({ unlistModal: true })} >
                    <Trash size={20} /> Delete Product
                  </Button>
                </ModalFooter>
              </ModalContent>
            </ModalDialog>
          </Modal>
          {!loading && !error ? 
            <DeleteProduct opened={this.state.unlistModal} onClose={() => this.setState({ unlistModal: false })}
            id={this.props.id} onDelete={this._handleDelete}/> : null
          }
        </Fragment>
        )}
      </Query>
    );
  }
}