import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Modal, ModalDialog, ModalContent, ModalCloseButton, ModalHeader, 
  Typography, ModalBody, ModalFooter, Button } from '@smooth-ui/core-sc'
import gql from 'graphql-tag';
import { ApolloConsumer } from 'react-apollo';
import Loader from './../../components/loader';

const Container = styled.div`
  display: flex;
  align-items: center;
  background-color: #f7f8f9;
  padding: 15px;
  cursor: pointer;
  
  &:hover{
    background-color: #fff;
  }
`;

const Avatar = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #d9d9d9;
`;

const Text = styled.span`
  color: #222;
  font-size: 18px;
  margin-left: 15px;
`;

const GET_USER = gql`
  query($user: String!){
    getUser(user: $user){
      _id
      name
      email
      village
      tehsil
      district
      state
      phone
    }
  }
`

export default class ListItem extends React.Component {

  state = {
    opened: false,
    user: {}
  }

  _handleClick = async client => {
    this.setState({ opened: true, loading: true });

    try{
      const { data } = await client.query({
        query: GET_USER,
        variables: {
          user: this.props.id
        }
      });
      
      this.setState({ loading: false, user: data.getUser })
    } catch (err){
      console.log(err)
      this.setState({ hasErr: true })
    }
  }

  render(){
    return (
    <ApolloConsumer>
      {client => (
        <Container onClick={() => this._handleClick(client)}>
          <Avatar>{this.props.name[0]}</Avatar>
          <Text>{this.props.name}</Text>
            <Modal opened={this.state.opened} onClose={() => this.setState({ opened: false })}>
              <ModalDialog>
                <ModalContent>
                  <ModalCloseButton />
                  <ModalHeader>
                    <Typography variant="h5" style={{color: '#343434'}}>User Details</Typography>
                  </ModalHeader>
                  <ModalBody>
                    {
                      this.state.loading ? <Loader /> :
                      <Fragment>
                        <p>Name:  {this.state.user.name}</p>
                        <p>Email: {this.state.user.email}</p>
                        <p>Phone: {this.state.user.phone}</p>
                        <p>Village: {this.state.user.village}</p>
                        <p>Tehsil: {this.state.user.tehsil}</p>
                        <p>District: {this.state.user.district}</p>
                        <p>State: {this.state.user.state}</p>
                      </Fragment>
                    }
                  </ModalBody>
                  <ModalFooter>
                    <Button variant="secondary" onClick={() => this.setState({ opened: false })}>
                      Ok
                    </Button>
                  </ModalFooter>
                </ModalContent>
              </ModalDialog>
            </Modal>
          </Container>
        )}
      </ApolloConsumer>
    )
  }
}

ListItem.defaultProps = {
  id: "undefined",
  name: 'Unnamed',
  email: 'No Email'
}