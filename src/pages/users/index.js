import React from 'react';
import { Typography } from '@smooth-ui/core-sc';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';
import ContainerHeader from './../../components/containerHeader';
import Container from './../../components/container';
import ListItem from './listItem';
import Loader from './../../components/loader';

const GET_USERS = gql`
  {
    getUsers{
      _id,
      name
    }
  }
`

export default class Users extends React.Component {

  render(){
    return (
      <Container>
        <ContainerHeader>
          <Typography variant="h4" style={{color: '#343434'}}>Users</Typography>
        </ContainerHeader>

        <Query query={GET_USERS} pollInterval={5000}>
          {({ loading, data, error }) => {
            if(loading) return <Loader />
            if(error) return <p>{error.message}</p>

            return data.getUsers.map(user => <ListItem name={user.name} email={user.email} id={user._id} key={user._id} />) 
          }}
        </Query>
      </Container>
    );
  }
}