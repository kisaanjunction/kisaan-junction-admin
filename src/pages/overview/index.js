import React from 'react';
import styled from 'styled-components';
import { Typography, Select } from '@smooth-ui/core-sc';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';
import ContainerHeader from './../../components/containerHeader';
import Container from './../../components/container';
import Loader from './../../components/loader';
// const LineChart = require("react-chartjs").Line;

// var chartData = {
// 	labels: ["January", "February", "March", "April", "May", "June", "July"],
// 	datasets: [
// 		{
// 			label: "My First dataset",
// 			fillColor: "transparent",
// 			strokeColor: "#12902c",
// 			pointColor: "#12902a",
// 			pointStrokeColor: "#fff",
// 			pointHighlightFill: "#fff",
// 			pointHighlightStroke: "rgba(220,220,220,1)",
// 			data: [65, 59, 80, 81, 56, 55, 40]
// 		}
// 	]
// };

const Grid = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const Card = styled.div`
  background-color: #fff;
  box-shadow: 0 0 15px #ccc;
  border-radius: 10px;
  padding: 10px 20px;
  margin: 10px 15px;
  min-width: 280px;
  display: flex;
  flex-direction: column;
  align-items: center;
  flex: 1;
`;

const Little = styled.span`
  color: #666;
  align-self: flex-start;
`

const Title = styled.span`
  color: #222;
  font-size: 34px;
  margin: 20px 10px;
  display: block;
  color: #12902c;
`;

const GET_OVERVIEW = gql`
  query($period: Period){
    getOverview(period: $period) {
      users
      products
      categories
    }
  }
`

export default class Overview extends React.Component {

  state = {
    period: 'ALL'
  }

  render(){
    return (
      <Container>
        <ContainerHeader>
          <Typography variant="h4" style={{color: '#343434'}}>Overview</Typography>

          <Select onChange={e => this.setState({ period: e.target.value })}>
            <option value="ALL">All Time</option>
            <option value="LAST24HOURS">Last 24 Hours</option>
            <option value="LAST7DAYS">Last 7 Days</option>
            <option value="LAST30DAYS">Last 30 Days</option>
          </Select>
        </ContainerHeader>

        <Query query={GET_OVERVIEW} variables={{period: this.state.period}}
          pollInterval={5000}>
          {({ data, error, loading }) => {
            if(loading) return <Loader />
            if(error) return <p>{error.message}</p>
            return (
              <Grid>
                <Card>
                <Little>Total Users</Little>
                <Title>{data.getOverview.users}</Title>
              </Card>
              <Card>
                <Little>Total Products Listed</Little>
                <Title>{data.getOverview.products}</Title>
              </Card>
              <Card>
                <Little>Total Categories</Little>
                <Title>{data.getOverview.categories}</Title>
              </Card>
            </Grid>
            )
          }}
        </Query>

        {/* <ContainerHeader>
          <Typography variant="h4" style={{color: '#343434'}}>Insights</Typography>
        </ContainerHeader>
        
        <div style={{overflow: 'auto', padding: 20}}>
          <div>
            
            <LineChart data={chartData} options={{bezierCurve: false}} width="1000" height="250"/>
          </div>
        </div> */}
      </Container>
    );
  }
}