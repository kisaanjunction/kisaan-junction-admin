import { theme } from '@smooth-ui/core-sc'

export default {
  ...theme,
  primary: '#343434',
  secondary: '#e6b31e'
} 